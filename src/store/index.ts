import VuexStore from './vuex';
import PiniaStore from './pinia';

export default {
    VuexStore,
    PiniaStore
};
