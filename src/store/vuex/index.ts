const _ = require("lodash");
import utils from "../utils";
import BaseStore from "../_base_store";
import {
	IVueXStore,
	IBaseState,
	IBaseActions,
	IBaseGetters,
	IBaseMutations,
} from "../../types";

// Store types
const coreTypes = {
	STORE_GET_INDEX_CONFIG: `STORE_GET_INDEX_CONFIG`,
	STORE_GET_FORM_CONFIG: `STORE_GET_FORM_CONFIG`,
	STORE_GET: `STORE_GET`,
	STORE_SET: `STORE_SET`,
	STORE_SET_APPENDS_DATA: "STORE_SET_APPENDS_DATA",
	STORE_GET_ALL: `STORE_GET_ALL`,
	STORE_SET_ALL: `STORE_SET_ALL`,
	STORE_SAVE: `STORE_SAVE`,
	STORE_DUPLICATE: `STORE_DUPLICATE`,
	STORE_CREATE: `STORE_CREATE`,
	STORE_UPDATE: `STORE_UPDATE`,
	STORE_IMPORT: `STORE_IMPORT`,
	STORE_EXPORT: `STORE_EXPORT`,
	STORE_DELETE: `STORE_DELETE`,
	STORE_UPDATE_STATS: `STORE_UPDATE_STATS`,
	STORE_CREATE_CACHE_GET: `STORE_CREATE_CACHE_GET`,
	STORE_CREATE_CACHE_UPDATE: `STORE_CREATE_CACHE_UPDATE`,
	STORE_CREATE_CACHE_REMOVE: `STORE_CREATE_CACHE_REMOVE`,
} as Record<string, string>;

class VuexStore extends BaseStore implements IVueXStore {
	coreTypes: typeof coreTypes;
	utils: any;

	constructor(props: any = {}) {
		super(props);
		this.utils = utils;
		this.coreTypes = coreTypes as typeof coreTypes;
	}

	/**
	 * Generate the modules dynamically
	 * @param {Object} modules
	 */
	generateModules<T = any>(modules: any): T {
		const allModules: any = {};
		Object.keys(modules).forEach((module: string) => {
			const clone = _.clone(this, true);
			const moduleObject = modules[module](clone);
			moduleObject.log = this.log;
			moduleObject.app = this.app;
			moduleObject.api = this.api;
			allModules[module] = moduleObject as typeof moduleObject;
		});
		return allModules as typeof allModules;
	}

	types<T extends Record<string, string> = typeof coreTypes>(types: object): T {
		const extend = types instanceof Object ? types : {};
		const localTypes: any = {};
		Object.keys(this.coreTypes || {}).forEach((key) => {
			localTypes[key] = key;
		});
		this.allTypes = { ...localTypes, ...extend };
		return this.allTypes as T;
	}

	/**
	 * Generate the state for the store.
	 * For Vuex, the state is returned as an object.
	 */
	state<T = IBaseState>(state: object = {}, exclusive: boolean = false): T {
		const extend = state instanceof Object ? state : {};
		const baseState: T =
			exclusive === true
				? ({} as T)
				: ({
						config: {
							index: null,
							form: null,
						},
						status: {
							data: ``,
							loading: false,
						},
						appendData: false,
						data: {
							data: [],
							all: [],
							total: 0,
						},
						all: [],
						imported: {
							data: [],
						},
						exported: {
							data: [],
						},
				  } as T);
		const result = { ...baseState, ...extend };
		return result as typeof result;
	}

	/**
	 * Generate getters for the store.
	 */
	getters<T = IBaseGetters>(getters?: object, exclusive: boolean = false): T {
		const extend = getters instanceof Object ? getters : {};
		const baseGetters: T =
			exclusive === true
				? ({} as T)
				: ({
						currentConfig: (state: IBaseState) => state.config,
						indexConfig: (state: IBaseState) => state.config.index,
						formConfig: (state: IBaseState) => state.config.form,
						currentData: (state: IBaseState) => state.data,
						isAllLoaded: (state: IBaseState): boolean =>
							state.data &&
							Array.isArray(state.data.data) &&
							state.data.data.length > 0,
						lastImported: (state: IBaseState) => state.imported,
				  } as T);
		const result = { ...baseGetters, ...extend };
		return result as typeof result;
	}

	/**
	 * Generate actions for the store.
	 */
	actions<T = IBaseActions>(
		actions?: object,
		_type: string = "unknown",
		exclusive?: boolean
	): T {
		const extend = actions instanceof Object ? { ...actions } : {};
		const api = this.api();
		const log = this.log();
		let type: string = _type;
		type = type[0] + type.slice(1);

		const baseActions: T =
			exclusive === true
				? ({} as T)
				: ({
						getIndexConfig: async function (
							this: IBaseState,
							params: any = {},
							force?: boolean
						) {
							log.info(`[Store: ${type}]: Get index config`, params);
							return await api.getIndexConfig(params, force);
						},
						getFormConfig: async function (
							this: IBaseState,
							params: any = {},
							force?: boolean
						) {
							log.info(`[Store: ${type}]: Get form config`, params);
							return await api.getFormConfig(params, force);
						},
						setAppendsData: async function (
							this: IBaseState,
							params?: boolean
						) {
							log.info(`[Store: ${type}]: Set appends data`, params);
							this.appendData = !!params;
							return this.appendData;
						},
						getAll: async function (this: IBaseState, params: any = {}) {
							log.info(`[Store: ${type}]: Get all`, params);
							return await api.getAll(params);
						},
						setAll: async function (this: IBaseState, data: any = {}) {
							log.info(`[Store: ${type}]: Set all`, data);
							this.data = data;
							this.all = this.all.concat(data);
							return this.all;
						},
						save: async function (this: IBaseState, data: any = {}) {
							log.info(`[Store: ${type}]: Save`, data);
							return await api.save(data);
						},
						getOne: async function (this: IBaseState, id: any = {}) {
							log.info(`[Store: ${type}]: Get one`, id);
							return await api.getOne(id);
						},
						setOne: async function (this: IBaseState, data: any = {}) {
							log.info(`[Store: ${type}]: Set one`, data);
							// Your implementation goes here.
							return data;
						},
						duplicate: async function (this: IBaseState, params?: any) {
							log.info(`[Store: ${type}]: Duplicate`, params);
							const response = await api.duplicate(params);
							return response.data;
						},
						import: async function (this: IBaseState, params?: any) {
							log.info(`[Store: ${type}]: Import`, params);
							const response = await api.import(params);
							return response.data;
						},
						export: async function (this: IBaseState, params?: any) {
							log.info(`[Store: ${type}]: Export`, params);
							const response = await api.export(params);
							return response.data;
						},
						delete: async function (this: IBaseState, params?: any) {
							log.info(`[Store: ${type}]: Delete`, params);
							const response = await api.delete(params);
							return response.data;
						},
						toggle: async function (
							this: IBaseState,
							params?: any,
							attr?: string
						) {
							log.info(`[Store: ${type}]: Toggle`, { attr, params });
							const response = await api.toggle(params);
							return response.data;
						},
				  } as T);
		const result = {
			type: () => type,
			log: () => log,
			api: () => api,
			...baseActions,
			...extend,
		};
		return result as typeof result;
	}

	/**
	 * Generate the mutations for the store
	 *
	 * @param {Object} mutations
	 * @param {Object} state
	 * @returns
	 */
	mutations<T = IBaseMutations>(mutations?: any, types?: any): T {
		const extend = mutations instanceof Object ? mutations : {};
		const _TYPES = this.types(types);
		const log = this.log();

		const result: T = {
			...{
				log() {
					return log;
				},
			},
			...{
				[_TYPES.STORE_UPDATE_STATS](state: any, stats: any) {
					return stats;
				},
				[_TYPES.STORE_GET_FORM_CONFIG](state: any, config: any) {
					state.config.form = config;
				},
				[_TYPES.STORE_GET_INDEX_CONFIG](state: any, config: any) {
					state.config.index = config;
				},
				[_TYPES.STORE_GET](state: any, data: any) {
					utils.addToStateData(state, data.result.data || data.result);
					return data;
				},
				[_TYPES.STORE_SET](state: any, data: any) {
					utils.addToStateData(state, data.result.data || data.result);
					return data;
				},
				[_TYPES.STORE_SAVE](state: any, data: any) {
					// Only update if this is a new item
					utils.addToStateData(state, data.result.data || data.result);
					return data;
				},
				[_TYPES.STORE_DUPLICATE](state: any, data: any) {
					// Only update if this is a new item
					utils.addToStateData(state, data.result.data || data.result);
					return data;
				},
				[_TYPES.STORE_IMPORT](state: any, data: any) {
					state.imported = data;
					if (data.data instanceof Array) {
						state.data.data.push([...data.data]);
						state.all = state.all.concat(data.data);
					}
					return data;
				},
				[_TYPES.STORE_EXPORT](state: any, data: any) {
					state.exported = data;
					return data;
				},
				[_TYPES.STORE_CREATE](state: any, data: any) {
					utils.addToStateData(state, data.result);
					return data;
				},
				[_TYPES.STORE_UPDATE](state: any, data: any) {
					utils.updateStateData(state, data.result);
					return data;
				},
				[_TYPES.STORE_SET_APPENDS_DATA](state: any, data: any) {
					state.appendData = !!data;
				},
				[_TYPES.STORE_GET_ALL](state: any, data: any) {
					if (state.appendData) {
						state.data = {
							...data.result,
							data: {
								data: state.data.data.concat(data.result.data),
							},
						};
					} else {
						state.data = data.result;
					}
					//   state.all = state.all.concat(data.result.data);
					return data;
				},
				[_TYPES.STORE_SET_ALL](state: any, data: any) {
					state.data = data;
					state.all = state.all.concat(data);
					return data;
				},
				[_TYPES.STORE_DELETE](state: any, data: any) {
					utils.removeFromStateData(state, data.params);
				},
				[_TYPES.STORE_CREATE_CACHE_GET](state: any) {
					return state.cachedCreateStore;
				},
				[_TYPES.STORE_CREATE_CACHE_UPDATE](
					state: any,
					data: any,
					type: string
				) {
					state.cachedCreateStore = Object.assign(
						state.cachedCreateStore || {},
						data
					);
					const realWindow: any = typeof window !== "undefined" ? window : null;
					if (realWindow && realWindow instanceof Object) {
						realWindow.localStorage.setItem(
							`cachedCreate${type}`,
							JSON.stringify(state.cachedCreateStore)
						);
					}
				},
				[_TYPES.STORE_CREATE_CACHE_REMOVE](state: any, type: string) {
					state.cachedCreateStore = null;
					const realWindow: any = typeof window !== "undefined" ? window : null;
					if (realWindow && realWindow instanceof Object) {
						realWindow.localStorage.removeItem(`cachedCreate${type}`);
					}
				},
			},
			...extend,
		};

		return result as typeof result;
	}
}

export default VuexStore;
