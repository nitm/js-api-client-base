import { expect } from "chai";
import sinon from "sinon";
import Store from "../src/store/pinia";

describe("Store", () => {
	let store: Store;
	let apiStub: any;
	let logStub: any;

	beforeEach(() => {
		store = new Store();
		apiStub = {
			getIndexConfig: sinon.stub(),
			getFormConfig: sinon.stub(),
			getAll: sinon.stub(),
			getOne: sinon.stub(),
			save: sinon.stub(),
			delete: sinon.stub(),
			toggle: sinon.stub(),
		};
		logStub = {
			info: sinon.stub(),
		};
		sinon.stub(store, "api").returns(apiStub);
		sinon.stub(store, "log").returns(logStub);
	});

	afterEach(() => {
		sinon.restore();
	});

	describe("generateModules", () => {
		it("should generate modules correctly", () => {
			const modules = {
				module1: () => () => ({}),
				module2: () => () => ({}),
			};
			const result = store.generateModules(modules);
			expect(Object.keys(result)).to.have.lengthOf(2);
			expect(result).to.have.all.keys("module1", "module2");
		});
	});

	describe("state", () => {
		it("should return default state when no arguments are provided", () => {
			const state = store.state();
			expect(state()).to.have.all.keys(
				"config",
				"status",
				"appendData",
				"data",
				"all",
				"imported",
				"exported"
			);
		});

		it("should merge provided state with default state", () => {
			const customState = { customKey: "customValue" };
			const state = store.state(customState);
			const result = state();
			expect(result).to.include(customState);
			expect(result).to.have.all.keys(
				"config",
				"status",
				"appendData",
				"data",
				"all",
				"imported",
				"exported",
				"customKey"
			);
		});

		it("should return only provided state when exclusive is true", () => {
			const customState = { customKey: "customValue" };
			const state = store.state(customState, true);
			const result = state();
			expect(result).to.deep.equal(customState);
		});
	});

	describe("actions", () => {
		it("should include base actions when not exclusive", () => {
			const actions = store.actions();
			expect(actions).to.have.all.keys(
				"type",
				"log",
				"api",
				"getIndexConfig",
				"getFormConfig",
				"setAppendsData",
				"getAll",
				"setAll",
				"getOne",
				"setOne",
				"getOneCached",
				"save",
				"duplicate",
				"import",
				"export",
				"delete",
				"toggle"
			);
		});

		it("should only include provided actions when exclusive", () => {
			const customActions = { customAction: () => {} };
			const actions = store.actions(customActions, "custom", true);
			expect(actions).to.have.all.keys("type", "log", "api", "customAction");
		});
	});

	describe("useModule", () => {
		it("should return a module object with store, log, app, and api properties", () => {
			const moduleDefinition = sinon.stub().returns(() => ({}));
			const result = store.useModule(moduleDefinition);
			expect(result).to.have.all.keys("store", "log", "app", "api");
		});

		it("should call the module definition with a cloned store instance", () => {
			const moduleDefinition = sinon.stub().returns(() => ({}));
			store.useModule(moduleDefinition);
			expect(moduleDefinition.calledOnce).to.equal(true);
			expect(moduleDefinition.firstCall.args[0]).to.not.equal(store);
		});
	});
});
