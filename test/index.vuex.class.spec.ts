import { expect } from 'chai';
import sinon from 'sinon';
import Store from '../src/store/vuex/index';
import utils from '../src/store/utils';

describe('Store', () => {
    let store: Store;
    let sandbox: sinon.SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        store = new Store();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('should initialize with default properties', () => {
        expect(store.utils).to.equal(utils);
        expect(store.coreTypes).to.be.an('object');
    });

    describe('generateModules', () => {
        it('should generate modules dynamically', () => {
            const modules = {
                testModule: (clone: any) => ({ ...clone, name: 'testModule' }),
            };
            const generatedModules = store.generateModules(modules);
            expect(generatedModules).to.have.property('testModule');
            expect(generatedModules.testModule).to.have.property('name', 'testModule');
        });
    });

    describe('types', () => {
        it('should extend core types with provided types', () => {
            const additionalTypes = { CUSTOM_TYPE: 'CUSTOM_TYPE' };
            const allTypes = store.types(additionalTypes);
            expect(allTypes).to.have.property('CUSTOM_TYPE', 'CUSTOM_TYPE');
        });
    });

    describe('state', () => {
        it('should generate a state object', () => {
            const state = store.state();
            expect(state).to.have.property('config');
            expect(state).to.have.property('status');
        });

        it('should extend the state object with provided state', () => {
            const additionalState = { customState: 'customValue' };
            const state = store.state(additionalState);
            expect(state).to.have.property('customState', 'customValue');
        });
    });

    describe('getters', () => {
        it('should generate getters', () => {
            const getters = store.getters({});
            expect(getters).to.have.property('currentData');
            expect(getters).to.have.property('currentConfig');
        });
    });

    describe('actions', () => {
        it('should generate actions', () => {
            const actions = store.actions({});
            expect(actions).to.have.property('getIndexConfig');
            expect(actions).to.have.property('log');
        });
    });

    describe('mutations', () => {
        it('should generate mutations', () => {
            const mutations = store.mutations({}, {}, {});
            expect(mutations).to.have.property(store.coreTypes.STORE_GET_INDEX_CONFIG);
            expect(mutations).to.have.property('log');
        });
    });
});