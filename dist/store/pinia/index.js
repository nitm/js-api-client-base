"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var utils_1 = __importDefault(require(".././utils"));
var _base_store_1 = __importDefault(require("../_base_store"));
var Store = /** @class */ (function (_super) {
    __extends(Store, _super);
    function Store(props) {
        if (props === void 0) { props = {}; }
        var _this = _super.call(this, props) || this;
        _this.utils = utils_1.default;
        return _this;
    }
    /**
     * Generate the modules dynamically
     * @param {Object} modules
     */
    Store.prototype.generateModules = function (modules) {
        var _this = this;
        var allModules = {};
        Object.keys(modules).map(function (module) {
            allModules[module] = _this.useModule(modules[module]);
        });
        return allModules;
    };
    /**
     * Prepare the modules dynamically
     * @param {Object} modules
     */
    Store.prototype.useModule = function (module) {
        var clone = _.clone(this, true);
        var store = module(clone)();
        var moduleObject = {
            store: store,
            log: this.log,
            app: this.app,
            api: this.api,
        };
        return moduleObject;
    };
    Store.prototype.types = function (types) {
        return types || {};
    };
    /**
     * Generate a state object
     *
     * @param {Object} state
     * @param {boolean} exclusive
     * @returns Function
     */
    Store.prototype.state = function (state, exclusive) {
        var extend = state instanceof Object ? state : {};
        var baseState = exclusive === true
            ? {}
            : {
                config: {
                    index: null,
                    form: null,
                },
                status: {
                    loading: false,
                    data: {},
                },
                appendData: false,
                data: {},
                all: [],
                imported: {
                    data: [],
                },
                exported: {
                    data: [],
                },
            };
        var result = __assign(__assign({}, baseState), extend);
        return function () { return result; };
    };
    /**
     * Generate the getters for the store
     * @aram {Object}
     * @param {boolean} exclusive
     * @returns object
     */
    Store.prototype.getters = function (getters, exclusive) {
        var extend = getters instanceof Object ? getters : {};
        var baseGetters = exclusive === true
            ? {}
            : {
                currentConfig: function (state) { return state.config; },
                indexConfig: function (state) { return state.config.index; },
                formConfig: function (state) { return state.config.form; },
                currentData: function (state) { return state.data; },
                isAllLoaded: function (state) {
                    return state.data instanceof Object &&
                        state.data.data instanceof Array &&
                        state.data.data.length > 0;
                },
                lastImported: function (state) { return state.imported; },
            };
        var $log = this.$log;
        var result = __assign(__assign({
            log: function () {
                return $log;
            },
        }, baseGetters), extend);
        return result;
    };
    /**
     * Generate the actions for the store
     *
     * @param {Object} actions
     * @param {string} type
     * @param {boolean} exclusive
     * @returns object
     */
    Store.prototype.actions = function (actions, _type, exclusive) {
        if (_type === void 0) { _type = "unknown"; }
        var api = this.api();
        var log = this.log();
        var type = _type;
        type = type[0] + type.toString().substring(1);
        var extend = actions instanceof Object ? __assign({}, actions) : {};
        var baseActions = exclusive === true
            ? {}
            : __assign({
                /**
                 * Get the index page config for the given type
                 * @param {IPiniaStoreState} this
                 * @param {Object} params
                 * @param {boolean} force
                 * @returns {Promise}
                 */
                getIndexConfig: function (params, force) {
                    if (params === void 0) { params = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        var forceGet, response, error_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    forceGet = force || true;
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 5, , 6]);
                                    if (!(!this.config.index || forceGet)) return [3 /*break*/, 3];
                                    log.info("[Store: ".concat(type, "]: GetIndexConfig"));
                                    return [4 /*yield*/, api.getIndexConfig(params)];
                                case 2:
                                    response = _a.sent();
                                    this.config.index = response.data.data;
                                    return [2 /*return*/, this.config.index];
                                case 3:
                                    log.info("[Store: ".concat(type, "]: Getting existing index config"), params);
                                    return [2 /*return*/, this.config.index];
                                case 4: return [3 /*break*/, 6];
                                case 5:
                                    error_1 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error getting index config"), error_1);
                                    throw error_1;
                                case 6: return [2 /*return*/];
                            }
                        });
                    });
                },
                /**
                 * Get the form config needed for creating or updating models
                 * @param {IPiniaStoreState} this
                 * @param {object} params
                 * @param {boolean} force
                 * @returns {Promise}
                 */
                getFormConfig: function (params, force) {
                    if (params === void 0) { params = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        var forceGet, response, error_2;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 4, , 5]);
                                    forceGet = force || true;
                                    if (!(!this.config.form || forceGet)) return [3 /*break*/, 2];
                                    log.info("[Store: ".concat(type, "]: GetFormConfig"));
                                    return [4 /*yield*/, api.getFormConfig(params)];
                                case 1:
                                    response = _a.sent();
                                    this.config.form = response.data.data;
                                    return [2 /*return*/, this.config.form];
                                case 2:
                                    log.info("[Store: ".concat(type, "]: Getting existing form config"), params);
                                    return [2 /*return*/, this.config.form];
                                case 3: return [3 /*break*/, 5];
                                case 4:
                                    error_2 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error getting form config"), error_2);
                                    throw error_2;
                                case 5: return [2 /*return*/];
                            }
                        });
                    });
                },
                /**
                 * Set the ability to append data to existing data
                 * @param {IPiniaStoreState} this
                 * @param {Object} params
                 * @returns {Promise}
                 */
                setAppendsData: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            log.info("[Store: ".concat(type, "]: Set Appends Data ").concat(type), params);
                            this.appendData = !!params;
                            return [2 /*return*/, this.appendData];
                        });
                    });
                },
                /**
                 * Get all of the items
                 * @param {IPiniaStoreState} this
                 * @param {Object} params
                 * @returns {Promise}
                 */
                getAll: function (params) {
                    if (params === void 0) { params = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        var response, error_3;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Get ").concat(type), params);
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    return [4 /*yield*/, api.getAll(params)];
                                case 2:
                                    response = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Got all ").concat(type), response.data);
                                    if (this.appendData) {
                                        this.data = __assign(__assign({}, response.data), { data: {
                                                data: this.data.data.concat(response.data),
                                            } });
                                    }
                                    else {
                                        this.data = response.data;
                                    }
                                    //   state.all = state.all.concat(data.result.data);
                                    return [2 /*return*/, this.data];
                                case 3:
                                    error_3 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error getting all"), error_3);
                                    throw error_3;
                                case 4: return [2 /*return*/];
                            }
                        });
                    });
                },
                /**
                 * Set the data for the given type
                 * @param {IPiniaStoreState} this
                 * @param {any} data
                 */
                setAll: function (data) {
                    if (data === void 0) { data = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            log.info("[Store: ".concat(type, "]: Set data ").concat(type), data);
                            this.data = data;
                            this.all = this.all.concat(data);
                            return [2 /*return*/, this.all];
                        });
                    });
                },
                /**
                 * Get the specific object with the given id
                 * @param {IPiniaStoreState} this
                 * @param {number|string} id
                 * @returns {Promise}
                 */
                getOne: function (id) {
                    if (id === void 0) { id = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        var response, result_1, error_4;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Get ").concat(type), id);
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 5, , 6]);
                                    log.info("[Store: ".concat(type, "]: Getting ").concat(type), id);
                                    if (!id) return [3 /*break*/, 3];
                                    return [4 /*yield*/, api.getOne(id)];
                                case 2:
                                    response = _a.sent();
                                    result_1 = Object.hasOwn(response.data, "meta")
                                        ? {
                                            meta: response.data.meta,
                                            data: response.data.data,
                                        }
                                        : response.data;
                                    utils_1.default.addToStateData(this.data.data, (result_1 === null || result_1 === void 0 ? void 0 : result_1.data) || result_1, true);
                                    return [2 /*return*/, result_1];
                                case 3: return [2 /*return*/, {}];
                                case 4: return [3 /*break*/, 6];
                                case 5:
                                    error_4 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error getting one"), error_4);
                                    throw error_4;
                                case 6: return [2 /*return*/];
                            }
                        });
                    });
                },
                /**
                 * Set the given object in the local store
                 * @param {IPiniaStoreState} this
                 * @param {any} data
                 * @returns {Promise}
                 */
                setOne: function (data) {
                    if (data === void 0) { data = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            log.info("[Store: ".concat(type, "]: Set one ").concat(type), data);
                            utils_1.default.addToStateData(this.data.data, data, true);
                            return [2 /*return*/, data];
                        });
                    });
                },
                /**
                 * Get the specific object with the given id in the lcoal cache
                 * @param {IPiniaStoreState} this
                 * @param {number|string} id
                 * @returns {Promise}
                 */
                getOneCached: function (id) {
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            log.info("[Store: ".concat(type, "]: GetOneCached"), id);
                            if (utils_1.default.findItemInState(this.data.data, id, true) === -1) {
                                return [2 /*return*/, this.getOneCached(id)];
                            }
                            else {
                                log.info("[Store: ".concat(type, "]: Getting existing ").concat(type), id);
                                return [2 /*return*/, utils_1.default.getItemInState(this.data.data, id, true)];
                            }
                            return [2 /*return*/];
                        });
                    });
                },
                /**
                 * Save the given data to the store
                 * @param {IPiniaStoreState} this
                 * @param {Object} params
                 * @returns {Promise}
                 */
                save: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response, data, error_5;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Save ").concat(type), params);
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    return [4 /*yield*/, api.save(params)];
                                case 2:
                                    response = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Saved ").concat(type), response);
                                    data = response.data;
                                    utils_1.default.addToStateData(this.data.data, (data === null || data === void 0 ? void 0 : data.data) || data, true);
                                    return [2 /*return*/, data];
                                case 3:
                                    error_5 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error saving"), error_5);
                                    throw error_5;
                                case 4: return [2 /*return*/];
                            }
                        });
                    });
                },
                /**
                 * Duplicate the given data to the store
                 * @param {IPiniaStoreState} this
                 * @param {Object} params
                 * @returns {Promise}
                 */
                duplicate: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response, data;
                        return __generator(this, function (_a) {
                            log.info("[Store: ".concat(type, "]: Duplicate ").concat(type), params);
                            try {
                                response = api.duplicate(params);
                                log.info("[Store: ".concat(type, "]: Duplicated ").concat(type), response);
                                data = response.data;
                                utils_1.default.addToStateData(this.data.data, (data === null || data === void 0 ? void 0 : data.data) || data, true);
                                return [2 /*return*/, data];
                            }
                            catch (error) {
                                log.info("[Store: ".concat(type, "]: Error duplicating"), error);
                                throw error;
                            }
                            return [2 /*return*/];
                        });
                    });
                },
                /**
                 * Import the given data into the store
                 * @param {IPiniaStoreState} this
                 * @param {Object} params
                 * @returns {Promise}
                 */
                import: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response, data, error_6;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Import"), params);
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    return [4 /*yield*/, api.import(params)];
                                case 2:
                                    response = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Imported"), response);
                                    data = response.data;
                                    this.imported = data;
                                    if (data.data instanceof Array) {
                                        this.data.data.push(__spreadArray([], data.data, true));
                                        this.all = this.all.concat(data.data);
                                    }
                                    return [2 /*return*/, data];
                                case 3:
                                    error_6 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error getting all"), error_6);
                                    throw error_6;
                                case 4: return [2 /*return*/];
                            }
                        });
                    });
                },
                /**
                 * Export the given data into the store
                 * @param {IPiniaStoreState} this
                 * @param {Object} params
                 * @returns {Promise}
                 */
                export: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response, data, error_7;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    log.info("[Store: ".concat(type, "]: Export"), params);
                                    return [4 /*yield*/, api.export(params)];
                                case 1:
                                    response = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Exported"), response);
                                    data = response.data;
                                    this.exported = data;
                                    return [2 /*return*/, this.exported];
                                case 2:
                                    error_7 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error getting all"), error_7);
                                    throw error_7;
                                case 3: return [2 /*return*/];
                            }
                        });
                    });
                },
                /**
                 * Delete the given data from the store
                 * @param {IPiniaStoreState} this
                 * @param {any} params
                 * @returns {Promise}
                 */
                delete: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response, error_8;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Delete ").concat(type), params);
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 5, , 6]);
                                    if (!params) return [3 /*break*/, 3];
                                    return [4 /*yield*/, api.delete(params)];
                                case 2:
                                    response = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Deleted ").concat(type), response.data);
                                    utils_1.default.removeFromStateData(this.data.data, params, true);
                                    return [2 /*return*/, response.data];
                                case 3: throw new Error("[Store: ".concat(type, "]: Null params"));
                                case 4: return [3 /*break*/, 6];
                                case 5:
                                    error_8 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error getting all"), error_8);
                                    throw error_8;
                                case 6: return [2 /*return*/];
                            }
                        });
                    });
                },
                /**
                 * Toggle the given data from the store
                 * @param {IPiniaStoreState} this
                 * @param {any} params
                 * @returns {Promise}
                 */
                toggle: function (params, attr) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response, result_2, error_9;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Toggle ").concat(type), { attr: attr, params: params });
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    return [4 /*yield*/, api.toggle(params)];
                                case 2:
                                    response = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Toggled ").concat(type), response);
                                    result_2 = Object.hasOwn(response.data, "meta")
                                        ? {
                                            meta: response.data.meta,
                                            data: response.data.data,
                                        }
                                        : response.data;
                                    utils_1.default.addToStateData(this.data.data, (result_2 === null || result_2 === void 0 ? void 0 : result_2.data) || result_2, true);
                                    return [2 /*return*/, result_2];
                                case 3:
                                    error_9 = _a.sent();
                                    log.info("[Store: ".concat(type, "]: Error getting all"), error_9);
                                    throw error_9;
                                case 4: return [2 /*return*/];
                            }
                        });
                    });
                },
            });
        var result = __assign(__assign({
            type: function () {
                return type;
            },
            log: function () {
                return log;
            },
            api: function () {
                return api;
            },
        }, baseActions), extend);
        return result;
    };
    return Store;
}(_base_store_1.default));
exports.default = Store;
