import BaseStore from "../_base_store";
import { IPiniaStore, PiniaStoreDefinition, PiniaModuleWrapper, IBaseActions, IBaseGetters, IStoreState, StateDataInterface } from "../../types";
declare class Store extends BaseStore implements IPiniaStore {
    utils: any;
    constructor(props?: any);
    /**
     * Generate the modules dynamically
     * @param {Object} modules
     */
    generateModules<T = PiniaModuleWrapper>(modules: any): Record<string, T>;
    /**
     * Prepare the modules dynamically
     * @param {Object} modules
     */
    useModule<T = any>(module: (a: T) => PiniaStoreDefinition): PiniaModuleWrapper;
    types<T = Record<string, string>>(types?: object): T;
    /**
     * Generate a state object
     *
     * @param {Object} state
     * @param {boolean} exclusive
     * @returns Function
     */
    state<T = any>(state?: object, exclusive?: boolean): () => IStoreState<T> & {
        [x: string]: any;
        data: StateDataInterface<T>;
        config: {
            [key: string]: any;
            index: any;
            /**
             * Get the index page config for the given type
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @param {boolean} force
             * @returns {Promise}
             */
            form: any; /**
             * Get the index page config for the given type
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @param {boolean} force
             * @returns {Promise}
             */
        };
        all: T[];
        appendData: boolean;
        imported: {
            [key: string]: any;
            data: any;
        };
        exported: {
            [key: string]: any;
            data: any;
        };
        status: {
            [key: string]: any;
            loading: boolean;
            data: any;
        };
    };
    /**
     * Generate the getters for the store
     * @aram {Object}
     * @param {boolean} exclusive
     * @returns object
     */
    getters<T = IBaseGetters>(getters?: object, exclusive?: boolean): T;
    /**
     * Generate the actions for the store
     *
     * @param {Object} actions
     * @param {string} type
     * @param {boolean} exclusive
     * @returns object
     */
    actions<T = any>(actions?: object, _type?: string, exclusive?: boolean): IBaseActions<T>;
}
export default Store;
