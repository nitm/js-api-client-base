"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = __importDefault(require("../utils"));
var _base_store_1 = __importDefault(require("../_base_store"));
// Store types
var coreTypes = {
    STORE_GET_INDEX_CONFIG: "STORE_GET_INDEX_CONFIG",
    STORE_GET_FORM_CONFIG: "STORE_GET_FORM_CONFIG",
    STORE_GET: "STORE_GET",
    STORE_SET: "STORE_SET",
    STORE_SET_APPENDS_DATA: "STORE_SET_APPENDS_DATA",
    STORE_GET_ALL: "STORE_GET_ALL",
    STORE_SET_ALL: "STORE_SET_ALL",
    STORE_SAVE: "STORE_SAVE",
    STORE_DUPLICATE: "STORE_DUPLICATE",
    STORE_CREATE: "STORE_CREATE",
    STORE_UPDATE: "STORE_UPDATE",
    STORE_IMPORT: "STORE_IMPORT",
    STORE_EXPORT: "STORE_EXPORT",
    STORE_DELETE: "STORE_DELETE",
    STORE_UPDATE_STATS: "STORE_UPDATE_STATS",
    STORE_CREATE_CACHE_GET: "STORE_CREATE_CACHE_GET",
    STORE_CREATE_CACHE_UPDATE: "STORE_CREATE_CACHE_UPDATE",
    STORE_CREATE_CACHE_REMOVE: "STORE_CREATE_CACHE_REMOVE",
};
var VuexStore = /** @class */ (function (_super) {
    __extends(VuexStore, _super);
    function VuexStore(props) {
        if (props === void 0) { props = {}; }
        var _this = _super.call(this, props) || this;
        _this.utils = utils_1.default;
        _this.coreTypes = coreTypes;
        return _this;
    }
    /**
     * Generate the modules dynamically
     * @param {Object} modules
     */
    VuexStore.prototype.generateModules = function (modules) {
        var allModules = {};
        Object.keys(modules).forEach(function (module) {
            // Add your module initialization logic here.
            allModules[module] = modules[module];
        });
        return allModules;
    };
    VuexStore.prototype.types = function (types) {
        var extend = types instanceof Object ? types : {};
        var localTypes = {};
        Object.keys(this.coreTypes || {}).forEach(function (key) {
            localTypes[key] = key;
        });
        this.allTypes = __assign(__assign({}, localTypes), extend);
        return this.allTypes;
    };
    /**
     * Generate the state for the store.
     * For Vuex, the state is returned as an object.
     */
    VuexStore.prototype.state = function (state, exclusive) {
        if (state === void 0) { state = {}; }
        if (exclusive === void 0) { exclusive = false; }
        var extend = state instanceof Object ? state : {};
        var baseState = exclusive === true
            ? {}
            : {
                config: {
                    index: null,
                    form: null,
                },
                status: {
                    data: "",
                    loading: false,
                },
                appendData: false,
                data: {
                    data: [],
                    all: [],
                    total: 0,
                },
                all: [],
                imported: {
                    data: [],
                },
                exported: {
                    data: [],
                },
            };
        var result = __assign(__assign({}, baseState), extend);
        return result;
    };
    /**
     * Generate getters for the store.
     */
    VuexStore.prototype.getters = function (getters, exclusive) {
        if (exclusive === void 0) { exclusive = false; }
        var extend = getters instanceof Object ? getters : {};
        var baseGetters = exclusive === true
            ? {}
            : {
                currentConfig: function (state) { return state.config; },
                indexConfig: function (state) { return state.config.index; },
                formConfig: function (state) { return state.config.form; },
                currentData: function (state) { return state.data; },
                isAllLoaded: function (state) {
                    return state.data && Array.isArray(state.data.data) && state.data.data.length > 0;
                },
                lastImported: function (state) { return state.imported; },
            };
        var result = __assign(__assign({}, baseGetters), extend);
        return result;
    };
    /**
     * Generate actions for the store.
     */
    VuexStore.prototype.actions = function (actions, _type, exclusive) {
        if (_type === void 0) { _type = "unknown"; }
        var extend = actions instanceof Object ? __assign({}, actions) : {};
        var api = this.api();
        var log = this.log();
        var type = _type;
        type = type[0] + type.slice(1);
        var baseActions = exclusive === true
            ? {}
            : {
                getIndexConfig: function (params, force) {
                    if (params === void 0) { params = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Get index config"), params);
                                    return [4 /*yield*/, api.getIndexConfig(params, force)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                },
                getFormConfig: function (params, force) {
                    if (params === void 0) { params = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Get form config"), params);
                                    return [4 /*yield*/, api.getFormConfig(params, force)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                },
                setAppendsData: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            log.info("[Store: ".concat(type, "]: Set appends data"), params);
                            this.appendData = !!params;
                            return [2 /*return*/, this.appendData];
                        });
                    });
                },
                getAll: function (params) {
                    if (params === void 0) { params = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Get all"), params);
                                    return [4 /*yield*/, api.getAll(params)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                },
                setAll: function (data) {
                    if (data === void 0) { data = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            log.info("[Store: ".concat(type, "]: Set all"), data);
                            this.data = data;
                            this.all = this.all.concat(data);
                            return [2 /*return*/, this.all];
                        });
                    });
                },
                save: function (data) {
                    if (data === void 0) { data = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Save"), data);
                                    return [4 /*yield*/, api.save(data)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                },
                getOne: function (id) {
                    if (id === void 0) { id = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Get one"), id);
                                    return [4 /*yield*/, api.getOne(id)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                },
                setOne: function (data) {
                    if (data === void 0) { data = {}; }
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            log.info("[Store: ".concat(type, "]: Set one"), data);
                            // Your implementation goes here.
                            return [2 /*return*/, data];
                        });
                    });
                },
                duplicate: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Duplicate"), params);
                                    return [4 /*yield*/, api.duplicate(params)];
                                case 1:
                                    response = _a.sent();
                                    return [2 /*return*/, response.data];
                            }
                        });
                    });
                },
                import: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Import"), params);
                                    return [4 /*yield*/, api.import(params)];
                                case 1:
                                    response = _a.sent();
                                    return [2 /*return*/, response.data];
                            }
                        });
                    });
                },
                export: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Export"), params);
                                    return [4 /*yield*/, api.export(params)];
                                case 1:
                                    response = _a.sent();
                                    return [2 /*return*/, response.data];
                            }
                        });
                    });
                },
                delete: function (params) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Delete"), params);
                                    return [4 /*yield*/, api.delete(params)];
                                case 1:
                                    response = _a.sent();
                                    return [2 /*return*/, response.data];
                            }
                        });
                    });
                },
                toggle: function (params, attr) {
                    return __awaiter(this, void 0, void 0, function () {
                        var response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    log.info("[Store: ".concat(type, "]: Toggle"), { attr: attr, params: params });
                                    return [4 /*yield*/, api.toggle(params)];
                                case 1:
                                    response = _a.sent();
                                    return [2 /*return*/, response.data];
                            }
                        });
                    });
                },
            };
        var result = __assign(__assign({ type: function () { return type; }, log: function () { return log; }, api: function () { return api; } }, baseActions), extend);
        return result;
    };
    /**
     * Generate the mutations for the store
     *
     * @param {Object} mutations
     * @param {Object} state
     * @returns
     */
    VuexStore.prototype.mutations = function (mutations, types) {
        var _a;
        var extend = mutations instanceof Object ? mutations : {};
        var _TYPES = this.types(types);
        var log = this.log();
        var result = __assign(__assign({
            log: function () {
                return log;
            },
        }, (_a = {},
            _a[_TYPES.STORE_UPDATE_STATS] = function (state, stats) {
                return stats;
            },
            _a[_TYPES.STORE_GET_FORM_CONFIG] = function (state, config) {
                state.config.form = config;
            },
            _a[_TYPES.STORE_GET_INDEX_CONFIG] = function (state, config) {
                state.config.index = config;
            },
            _a[_TYPES.STORE_GET] = function (state, data) {
                utils_1.default.addToStateData(state, data.result.data || data.result);
                return data;
            },
            _a[_TYPES.STORE_SET] = function (state, data) {
                utils_1.default.addToStateData(state, data.result.data || data.result);
                return data;
            },
            _a[_TYPES.STORE_SAVE] = function (state, data) {
                // Only update if this is a new item
                utils_1.default.addToStateData(state, data.result.data || data.result);
                return data;
            },
            _a[_TYPES.STORE_DUPLICATE] = function (state, data) {
                // Only update if this is a new item
                utils_1.default.addToStateData(state, data.result.data || data.result);
                return data;
            },
            _a[_TYPES.STORE_IMPORT] = function (state, data) {
                state.imported = data;
                if (data.data instanceof Array) {
                    state.data.data.push(__spreadArray([], data.data, true));
                    state.all = state.all.concat(data.data);
                }
                return data;
            },
            _a[_TYPES.STORE_EXPORT] = function (state, data) {
                state.exported = data;
                return data;
            },
            _a[_TYPES.STORE_CREATE] = function (state, data) {
                utils_1.default.addToStateData(state, data.result);
                return data;
            },
            _a[_TYPES.STORE_UPDATE] = function (state, data) {
                utils_1.default.updateStateData(state, data.result);
                return data;
            },
            _a[_TYPES.STORE_SET_APPENDS_DATA] = function (state, data) {
                state.appendData = !!data;
            },
            _a[_TYPES.STORE_GET_ALL] = function (state, data) {
                if (state.appendData) {
                    state.data = __assign(__assign({}, data.result), { data: {
                            data: state.data.data.concat(data.result.data),
                        } });
                }
                else {
                    state.data = data.result;
                }
                //   state.all = state.all.concat(data.result.data);
                return data;
            },
            _a[_TYPES.STORE_SET_ALL] = function (state, data) {
                state.data = data;
                state.all = state.all.concat(data);
                return data;
            },
            _a[_TYPES.STORE_DELETE] = function (state, data) {
                utils_1.default.removeFromStateData(state, data.params);
            },
            _a[_TYPES.STORE_CREATE_CACHE_GET] = function (state) {
                return state.cachedCreateStore;
            },
            _a[_TYPES.STORE_CREATE_CACHE_UPDATE] = function (state, data, type) {
                state.cachedCreateStore = Object.assign(state.cachedCreateStore || {}, data);
                var realWindow = typeof window !== "undefined" ? window : null;
                if (realWindow && realWindow instanceof Object) {
                    realWindow.localStorage.setItem("cachedCreate".concat(type), JSON.stringify(state.cachedCreateStore));
                }
            },
            _a[_TYPES.STORE_CREATE_CACHE_REMOVE] = function (state, type) {
                state.cachedCreateStore = null;
                var realWindow = typeof window !== "undefined" ? window : null;
                if (realWindow && realWindow instanceof Object) {
                    realWindow.localStorage.removeItem("cachedCreate".concat(type));
                }
            },
            _a)), extend);
        return result;
    };
    return VuexStore;
}(_base_store_1.default));
exports.default = VuexStore;
