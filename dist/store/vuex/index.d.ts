import BaseStore from "../_base_store";
import { IVueXStore, IBaseState, IBaseActions, IBaseGetters, IBaseMutations } from "../../types";
declare const coreTypes: Record<string, string>;
declare class VuexStore extends BaseStore implements IVueXStore {
    coreTypes: typeof coreTypes;
    utils: any;
    constructor(props?: any);
    /**
     * Generate the modules dynamically
     * @param {Object} modules
     */
    generateModules<T = any>(modules: any): T;
    types<T extends Record<string, string> = typeof coreTypes>(types: object): T;
    /**
     * Generate the state for the store.
     * For Vuex, the state is returned as an object.
     */
    state<T = IBaseState>(state?: object, exclusive?: boolean): T;
    /**
     * Generate getters for the store.
     */
    getters<T = IBaseGetters>(getters?: object, exclusive?: boolean): T;
    /**
     * Generate actions for the store.
     */
    actions<T = IBaseActions>(actions?: object, _type?: string, exclusive?: boolean): T;
    /**
     * Generate the mutations for the store
     *
     * @param {Object} mutations
     * @param {Object} state
     * @returns
     */
    mutations<T = IBaseMutations>(mutations?: any, types?: any): T;
}
export default VuexStore;
