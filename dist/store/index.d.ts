import VuexStore from './vuex';
import PiniaStore from './pinia';
declare const _default: {
    VuexStore: typeof VuexStore;
    PiniaStore: typeof PiniaStore;
};
export default _default;
