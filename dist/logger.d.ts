declare const logger: {
    create(level?: string): import("js-logger").GlobalLogger;
};
export default logger;
