export interface IApi<T = any> {
    type: string;
    log: () => ILogInterface;
    $app: T;
    $api: IJsApi | any;
    utils: IApiUtils;
    app: <T = any>() => T | object;
    api: <T = any>() => T | object;
    setApi: <T = any>(app: IJsApi | any, headers?: any) => T | any;
    setApp: <T = any>(app: any) => T | any;
    setLogger: (logger: ILogInterface | any, level: string) => void;
    getUrl: (index: string, _endPoint?: any, _params?: any, basePath?: any, app?: any) => string;
    getBasePath: (action?: string, app?: any, params?: any) => string;
    createProxy: () => typeof Proxy;
    getFormConfig: <T = any>(params?: any, options?: any) => Promise<T | any>;
    getIndexConfig: <T = any>(params?: any, options?: any) => Promise<T | any>;
    getOne: <T = any>(params?: any, options?: any) => Promise<T | any>;
    getAll: <T = any>(params?: any, options?: any) => Promise<T | any>;
    save: <T = any>(params?: any, options?: any) => Promise<T | any>;
    import: <T = any>(params?: any, options?: any) => Promise<T | any>;
    export: <T = any>(params?: any, options?: any) => Promise<T | any>;
    delete: <T = any>(params?: any, options?: any) => Promise<T | any>;
    toggle: <T = any>(params?: any, options?: any) => Promise<T | any>;
    duplicate: <T = any>(params?: any, options?: any) => Promise<T | any>;
}
export interface ILogInterface {
    log: (message: string) => void;
    info: (message: string) => void;
    warn: (message: string) => void;
    error: (message: string) => void;
    trace: (message: string) => void;
    debug: (message: string) => void;
}
export interface IUtils {
    addToStateData: (state: Array<any> | StateInterface | IBaseState, item: any, stateIsTarget?: boolean, push?: boolean) => void;
    updateStateData: (state: Array<any> | StateInterface | IBaseState, item: any, stateIsTarget?: boolean, addToState?: boolean) => void;
    removeFromStateData: (state: Array<any> | StateInterface | IBaseState, item: any, stateIsTarget?: boolean) => void;
    findItemInState: (state: Array<any> | StateInterface | IBaseState, item: any, stateIsTarget?: boolean) => number;
    getItemInState: (state: Array<any> | StateInterface | IBaseState, item: any, stateIsTarget?: boolean) => void;
}
export interface IApiUtils {
    resolveError: <T = any>(error: any) => T | any;
    flattenObject: <T = any>(obj: any, prefix?: any) => T | any;
    createFormData: (data: any) => FormData;
    printFormData: (formData: any) => void;
    objectValues: <T = any>(obj: any) => T[] | any[];
    getCookie: <T = any>(name: string) => T | any;
}
export interface IBaseStore<AppT = any> {
    $log?: ILogInterface | any;
    $app?: AppT;
    $api?: IApi | any;
    coreTypes?: Record<string, string>;
    allTypes?: Record<string, string>;
    utils?: IUtils;
    app: () => AppT;
    api: () => IApi | any;
    setApi: (api: IApi) => void;
    setApp: (app: any) => void;
    setLogger: (logger: any) => void;
    createLogger: (level: string) => void;
}
export interface IVueXStore extends IBaseStore {
    generateModules: <T = any>(modules: any) => T;
    types: (types?: any) => object;
    state: (state?: object, exclusive?: boolean) => IBaseState;
    getters: (getters: object, exclusive?: boolean) => IBaseGetters;
    actions: (actions: object, type: string, exclusive?: boolean) => IBaseActions;
}
export interface IPiniaStore extends IBaseStore {
    useModule: <T = any>(module: (a: T) => PiniaStoreDefinition) => PiniaModuleWrapper;
    generateModules: <T = any | PiniaModuleWrapper>(modules: any) => Record<string, T>;
    types: (types: any) => Record<string, string>;
    state: <T = any>(state?: object, exclusive?: boolean) => () => IBaseState<T>;
    getters: (getters: object, exclusive?: boolean) => IBaseGetters;
    actions: (actions: object, type: string, exclusive?: boolean) => IBaseActions;
    log: <T = any>() => ILogInterface | T;
    utils: IUtils;
}
export interface IStoreState<T = any> {
    data: StateDataInterface<T>;
    config: {
        index: any;
        form: any;
        [key: string]: any;
    };
    all: Array<T>;
    appendData: boolean;
    imported: {
        data: any;
        [key: string]: any;
    };
    exported: {
        data: any;
        [key: string]: any;
    };
    status: {
        loading: boolean;
        data: any;
        [key: string]: any;
    };
    [key: string]: any;
}
export interface StateDataInterface<T = any> {
    data: Array<T>;
    all?: Array<T>;
    total?: number;
}
export interface StateInterface<T = any> {
    data: StateDataInterface<T>;
    total?: number;
    [key: string]: any;
}
export interface PiniaModuleWrapper<T = any> {
    store: PiniaStoreDefinition;
    log: ILogInterface | any;
    app: T | any;
    api: IJsApi | any;
}
/**
 * Return type of `defineStore()`. Function that allows instantiating a store.
 * From pinia.d.ts
 */
export interface IStoreDefinition {
    currentConfig?: any;
    indexConfig?: any;
    formConfig?: any;
    currentData?: any;
    isAllLoaded?: boolean;
    lastImported?: any;
    getOne: <T = any>(id: any) => Promise<T | any>;
    getOneCached: <T = any>(id: any) => Promise<T | any>;
    setOne: <T = any>(data: any) => Promise<T | any>;
    getAll: <T = any>(params?: any) => Promise<T | any>;
    setAll: <T = any>(data?: any) => Promise<T | any>;
    save: <T = any>(data: any) => Promise<T | any>;
    duplicate: <T = any>(params: any) => Promise<T | any>;
    import: <T = any>(params: any) => Promise<T | any>;
    export: <T = any>(params: any) => Promise<T | any>;
    delete: <T = any>(params: any) => Promise<T | any>;
    toggle: <T = any>(params: any) => Promise<T | any>;
    getIndexConfig: <T = any>(params?: any, force?: boolean) => Promise<T | any>;
    getFormConfig: <T = any>(params?: any, force?: boolean) => Promise<T | any>;
    setAppendsData: (appends: boolean) => void;
    [key: string]: any;
}
export interface IPiniaStoreState extends IStoreState {
    [key: string]: any;
}
/**
 * Return type of `defineStore()`. Function that allows instantiating a store.
 * From pinia.d.ts
 */
export interface PiniaStoreDefinition extends IStoreDefinition, IPiniaStoreState {
    /**
     * Returns a store, creates it if necessary.
     *
     * @param pinia - Pinia instance to retrieve the store
     * @param hot - dev only hot module replacement
     */
    <T = any>(pinia?: any | null | undefined, hot?: any): T | any;
    /**
     * Id of the store. Used by map helpers.
     */
    $id?: any;
}
export interface IVuexStoreDefinition extends IStoreDefinition, IStoreState {
}
export interface IStoreOptions<T = any> {
    app: T;
    api: IApi;
    logger: ILogInterface | any;
    [key: string]: any;
}
export interface IJsApi<T = any> {
    app: T;
    api: IJsApi;
    log: ILogInterface | any;
}
export interface IBaseState<T = any> extends IStoreState<T> {
    [key: string]: any;
}
export interface IBaseActions<T = any> extends IBaseState<T> {
    getIndexConfig?: <T = any>(params?: any, force?: boolean) => Promise<T | any>;
    getFormConfig?: <T = any>(params?: any, force?: boolean) => Promise<T | any>;
    getOne?: <T = any>(id: any) => Promise<T | any>;
    getAll?: <T = any>(params?: any) => Promise<T | any>;
    save?: <T = any>(data: any) => Promise<T | any>;
    duplicate?: <T = any>(params: any) => Promise<T | any>;
    import?: <T = any>(params: any) => Promise<T | any>;
    export?: <T = any>(params: any) => Promise<T | any>;
    delete?: <T = any>(params: any) => Promise<T | any>;
    toggle?: <T = any>(params: any) => Promise<T | any>;
    [key: string]: any;
}
export interface IBaseGetters {
    currentConfig?: <T = any>(state: IBaseState) => T | any;
    indexConfig?: <T = any>(state: IBaseState) => T | any;
    formConfig?: <T = any>(state: IBaseState) => T | any;
    currentData?: <T = any>(state: IBaseState) => T | any;
    isAllLoaded?: (state: IBaseState) => boolean;
    lastImported?: <T = any>(state: IBaseState) => T | any;
    [key: string]: any;
}
export interface IBaseMutations {
    setIndexConfig?: (state: IBaseState, data: any) => void;
    setFormConfig?: (state: IBaseState, data: any) => void;
    setCurrentData?: (state: IBaseState, data: any) => void;
    setAllLoaded?: (state: IBaseState, data: boolean) => void;
    setLastImported?: (state: IBaseState, data: any) => void;
    [key: string]: any;
}
