"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseStore = exports.BaseApi = void 0;
var services_1 = __importDefault(require("./services"));
var store_1 = __importDefault(require("./store"));
var services_2 = require("./services");
Object.defineProperty(exports, "BaseApi", { enumerable: true, get: function () { return __importDefault(services_2).default; } });
var store_2 = require("./store");
Object.defineProperty(exports, "BaseStore", { enumerable: true, get: function () { return __importDefault(store_2).default; } });
exports.default = {
    BaseApi: services_1.default,
    BaseStore: store_1.default
};
__exportStar(require("./types"), exports);
